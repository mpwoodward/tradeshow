#!/usr/bin/env bash

echo "Running apt-get update ..."
apt-get update

echo "Installing required Python and other packages ..."
apt-get install -y build-essential python3-pip python3-dev python-psycopg2 libssl-dev libjpeg8-dev libjpeg62 libtiff4-dev zlib1g-dev libfreetype6-dev liblcms2-dev libwebp-dev libaio1 g++ git wget curl vim zip unzip libpq-dev postgresql postgresql-contrib openssl

apt-get install -y libffi6 libffi-dev libxml2 libxml2 libxslt1-dev python-libxslt1 libxslt1-dev libcairo2 libcairo2-dev pango1.0-dev gir1.2-coglpango-1.0

if [ ! -d /var/log/ecomm ]
then
    echo "Creating log directory /var/log/ecomm ..."
    mkdir /var/log/ecomm
    chmod -R 777 /var/log/ecomm
fi

if [ ! -f /vagrant/requirements_installed ]
then
    echo "Installing project-specific Python packages with pip3 ..."
    cd /vagrant
    pip3 install -r requirements.txt
    touch requirements_installed
fi

echo "Provisioning complete."
