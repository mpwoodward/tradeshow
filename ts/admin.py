from django.contrib import admin

from ts.models import Tradeshow, Boothmap, Mappoint, Product


admin.site.register(Tradeshow)
admin.site.register(Boothmap)
admin.site.register(Mappoint)
admin.site.register(Product)
