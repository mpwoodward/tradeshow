from django.db import models


class ProductManager(models.Manager):
    def get_by_tradeshow_id(self, tradeshow_id):
        return self.filter(mappoint__boothmap__tradeshow__id=tradeshow_id)


class Product(models.Model):
    name = models.CharField(max_length=50)

    objects = ProductManager()

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class Mappoint(models.Model):
    name = models.CharField(max_length=50)
    products = models.ManyToManyField(Product, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class Boothmap(models.Model):
    name = models.CharField(max_length=50)
    mappoints = models.ManyToManyField(Mappoint, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class Tradeshow(models.Model):
    name = models.CharField(max_length=50)
    boothmaps = models.ManyToManyField(Boothmap, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
