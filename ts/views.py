from django.shortcuts import render

from ts.models import Product, Tradeshow


def tradeshows(request):
    return render(request, 'tradeshows.html', {'tradeshows': Tradeshow.objects.all()})


def products_by_tradeshow(request, tradeshow_id=None):
    if tradeshow_id:
        tradeshow = Tradeshow.objects.get(id=tradeshow_id)
        products = Product.objects.get_by_tradeshow_id(tradeshow_id)
    else:
        tradeshow = None
        products = None

    return render(request, 'products_by_tradeshow.html', {'tradeshow': tradeshow, 'products': products})
