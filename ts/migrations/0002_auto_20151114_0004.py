# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ts', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='boothmap',
            name='mappoints',
            field=models.ManyToManyField(null=True, blank=True, to='ts.Mappoint'),
        ),
        migrations.AlterField(
            model_name='mappoint',
            name='products',
            field=models.ManyToManyField(null=True, blank=True, to='ts.Product'),
        ),
        migrations.AlterField(
            model_name='tradeshow',
            name='boothmaps',
            field=models.ManyToManyField(null=True, blank=True, to='ts.Boothmap'),
        ),
    ]
