# TRADESHOW SAMPLE APP #

Very simple application showing how to handle many-to-many relationships in all directions in Django.

### Example: Get All Products for a Tradeshow ###


```
#!python

>>> from ts.models import *
>>> Product.objects.get_by_tradeshow_id(2)
```